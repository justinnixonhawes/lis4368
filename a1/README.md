
1. git init - creates a new directory or initializes an existing one as a git
 directory

2. git status - displays information about the current directory such as tracked
 and untracked files, number of commits, and branch status

3. git add - adds contents of the current path to the stage for the next
 commit

4. git commit - sends added content to the repository

5. git push - updates remote content using local content 

6. git pull - incorporates changes from remote repository to local branch

7. git clone - creates a clone of the current directory   


![JDK install](img/jdk_install.png)
![Tomcat Image](img/tomcat.png)
[A1](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a1/)

[BitBucketStationLocations](bitbucket.org/justinnixonhawes/bitbucketstationlocations/src/master)