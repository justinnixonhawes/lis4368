# Project 1 README

## This project showcases data vaildation

Failed Validation example

![Failed Validation](img/valfail.png)

Passed Validation example

![Passed Validation](img/valpass.png)

* valid: 'fa fa-check' generates a check in the text box
* invalid: 'fa fa-times' generates an x in the text box
* validating: 'fa fa-refresh' generates a refresh icon
