|Valid Data|Passed Validation|
|---|---|
|![Validation](img/valid.png)|![passed validation](img/passed.png)|
|---|---|
|updated data|
|---|
|![Passed Validation](img/update.png)|


|Customer Table|
|---|
|![app table](img/display.png)|
|Modified Table|
|![modified app](img/modified.png)|


|The Database|
|---|
|![DB before changes](img/first.png)|
|Removing a record and adding another|
|![delete and add](img/prechange.png)|
|Modified record|
|![Modified record](img/postchange.png)|