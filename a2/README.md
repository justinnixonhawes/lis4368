[Hello](http://localhost:9999/hello)
![Hello](img/hello.png)


[Hello/index](http://localhost:9999/hello/index.html)
![Hello w/ index](img/index.png)


[Say Hello](http://localhost:9999/hello/sayhello)
![bookshop](img/sayhello.png)


[Yet Another Bookshop](http://localhost:9999/hello/querybook.html)
![Query Results](img/bookshop.png)


## Query Results
![Query Results](img/queryresults.png)