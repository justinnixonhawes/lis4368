![A3 ERD](img/a3.png "ERD based upon A3 Requirements")

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")
