Course work links:

1. [A1 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a1/README.md)
    * Install JDK
    * Install Tomcat
    * Provided screenshots of installations
    * Created a Bitbucket repo
    * Completed Bitbucket tutorials
    * provide Git command descriptions

2. [A2 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a2/README.md)
    * Deploy webapp
    * Create Index.html
    * Provided screenshots of web app pages
    * Provided screenshots of query results

3. [A3 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a3/README.md)
    * Created an ERD with MySQL Workbench
    * Forward Engineered ERD
    * Connected database to webapp

4. [A4 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a4/README.md)
    * Set up server side verification 
    * Set up a page in for successful verification and a page for failed verification
    * CRUD - Create, Read, Update, Delete

5. [A5 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/a5/README.md)
    * Connects to batabase
    * JSTL to prevent cross side scripting
    * Prepared statements to prevent sql injection

6. [P1 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/p1/README.md)
    * Created form centric webpage to accept new customers to match database entity 
    * Used regular expression to validate entered data

7. [P2 README.md](https://bitbucket.org/justinnixonhawes/lis4368/src/master/p2/README.md)
    * Used MVC framework to develop a web app.
    * Implemented CRUD functionality to the web app.
    * Prevent malicious actions by using JSTL and prepared statements.
